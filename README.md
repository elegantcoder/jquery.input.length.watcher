jQuery Input Length Watcher
===========================

## Description
jQuery Input Length Watcher 는 input 엘리먼트 value 길이를 시간차를 두고 체크합니다. 브라우저에 따라 한글을 입력하는 경우 onkeyup, onkeydown, onchange 등의 이벤트가 원하는 시점에 발생하지 않는 경우에 사용합니다. - Firefox, Android 일부 버전 등

## Usage
	// 이벤트 핸들러 정의
	$input.on('lengthChange', function () { console.log('input value length changed')});
	$input.watchLength().val('가나다')
	$input.unwatchLength()

	$input.watchLength().val('') //이벤트 실행 됨
	//또는

	// callback 전달
	$input.watchLength(function () { console.log('input value length changed')}).val('가나다');
	$input.unwatchLength()

	$input.watchLength().val('') //callback 실행되지 않음

### callback 전달과 이벤트 핸들러 정의의 차이
위 코드는 이벤트 핸들러 정의방식과 callback 전달 방식으로 구분되어 있습니다. 내부적으로는 callback으로 정의하더라도 이벤트 발생을 통해 처리하므로 큰 차이는 없습니다.

이벤트 핸들러로 정의하는 경우 .unwatchLength() 를 하더라도 이벤트 핸들러가 삭제되지 않습니다. 다시 .watchLength() 를 하면 이벤트 핸들러를 다시 걸지 않아도 해당 이벤트 핸들러가 실행됩니다.

callback으로 정의하는 경우 .unwatchLength() 를 하면 callback이 삭제되어 다시 .watchLength() 를 하더라도 해당 callback을 실행할 수 없습니다.

## 수정시 주의할 점
/test 아래의 테스트 케이스를 추가하시고, 모든 테스트가 통과하는 상태로 커밋/푸시 하시기 바랍니다. 